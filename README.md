# FastAPI-Test-Task


## Задача
Реализуйте REST-сервис просмотра текущей зарплаты и даты следующего
повышения. Из-за того, что такие данные очень важны и критичны, каждый
сотрудник может видеть только свою сумму. Для обеспечения безопасности, вам
потребуется реализовать метод где по логину и паролю сотрудника будет выдан
секретный токен, который действует в течение определенного времени. Запрос
данных о зарплате должен выдаваться только при предъявлении валидного токена.

## Требования к решению
Обязательные технические требования:
1. код размещен и доступен в публичном репозитории на GitLab;
2. оформлена инструкция по запуску сервиса и взаимодействию с проектом
(Markdown файл с использованием конструкций разметки от GitLab по
необходимости);
3. сервис реализован на FastAPI.
Необязательные технические требования (по возрастанию трудоемкости):
4. зависимости зафиксированы менеджером зависимостей poetry;
5. написаны тесты с использованием pytest;
6. реализована возможность собирать и запускать контейнер с сервисом в Docker.

## Подготовка к запуску
1. Заполните .env файл (пример .env_sample)
```
DB_HOST=postgres-container/localhost
DB_PORT= 5432
DB_NAME=fastapi_auth_db
DB_USER=fastapi_auth_user
DB_PASS=sample
SECRET_KEY = # run openssl rand -hex 32 to get a secret token_type. for example: "36505276e41bbd5f17a0cd1f8e86aaf2fb8ea15db00514b0ef7c057c4bb4c93c"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30
```
2. Укажите настройки PostgreSQL в файле database.ini (пример database_sample.ini)
```
[postgresql]
host=postgres-container
database=fastapi_auth_db
user=fastapi_auth_user
password=sample
```
   
## Запуск через Poetry
1. Установите зависимости
```
$ poetry install
```
2. Запустите сервис 
```
$ poetry run uvicorn src.main:app --reload
```

## Запуск через Docker:
1. Запустите сервис
```
$ docker-compose --env-file .env up

```

## Ссылки:
1. http://127.0.0.1:8000/docs
